This repository contains a simple website with the Price List component for the Protonmail website. It is using a simple GatsbyJs boilerplate and it is written in ReactJS with React Hooks and styled in SCSS. 

## 🚀 Quick start

1.  **Install the node modules.**

    Use the terminal to install the node modules inside the website root folder.

    ```shell
    #  install node modules
    npm install
    ```

1.  **Run the application.**

    Navigate into your the site’s directory and start it up.

    ```shell
    #  use the command gatsby develop or npm run develop to start the application in development mode
    npm run develop
    ```
    The app is now running at `http://localhost:8000`!

    _Note: You'll also see a second link: _`http://localhost:8000/___graphql`_. This is a tool you can use to experiment with querying data. 

## 🧐 What's inside?

The App's files can be found in the `src` folder

    ├── assets
    ├── components
    ├── layouts
    ├── pages
    ├── utils
1.  **`/assets`**: The assets folder contains the `scss` files 
2.  **`/components`**: Contains the 2 components that the website is using `Dropdown` and `PriceList` 
3.  **`/layouts`**: Layouts is empty as for this application there is no need of layouts.
4.  **`/pages`**: Contains 2 pages, `404.js` - which is a simple 404 page and `index.js` - which is the main page for the app
5.  **`/utils`**: Contains 2 files, `api.js` - holds the function that makes the call to the API and grabs the packages `utils.js` - holds 4 simple functions that are being used within the application
