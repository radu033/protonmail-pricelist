/**
 * Configure your Gatsby site with this file.
 *
 * See: https://www.gatsbyjs.com/docs/gatsby-config/
 */

module.exports = {
  /* Your site config here */
  plugins: [`gatsby-plugin-sass`],
  siteMetadata: { 
    packages: {
      free: {
        description: "The basic for private and secure communications"
      },
      plus: {
        description: "ProtonMail for professionals and businesses",
        features: "Supports folders, labels, filters, auto-reply, IMAP/SMTP and more"
      },
      professional: {
        description: "ProtonMail for professionals and businesses",
        features: "Catch all email, multi user management, priority support and more"
      },
      visionary: {
        support: "Priority support",
        description: "ProtonMail for families and small businesses",
        features: "Includes all features",
      },
    },
    timePeriod: [
      {
        name: "Monthly",
        value: 1
      },
      {
        name: "Annually",
        value: 12
      },
      {
        name: "2 Years",
        value: 24
      }
    ],
    currencies: [
      {
        name: "€ Euro",
        value: "€"
      },
      {
        name: "CHF",
        value: "CHF"
      },
      {
        name: "$ Usd",
        value: "$"
      }
    ]
  }
}
