import React from 'react'
import getPackages from '../utils/api.js';
import {convertAmount, convertSpace, optional, keyExists} from '../utils/utils';
import PropTypes from 'prop-types';

export default function PriceList ({currency, period, extra}) {
  const [loading, setLoading] = React.useState(true);
  const [error, setError] = React.useState(false);
  const [plans, setPlans] = React.useState([
    {
      ID:"flEG-ZOb28XR4sCGFCEpqQbF46HTVWYfTfKYUmV_wKKR3GsveN4HZCnkfodhelYylEp-fhjBbUPDMH348jfw==",
      Name:"free",
      Type:1,
      MaxDomains:0,
      MaxAddresses:1,
      MaxSpace:524288000,
      MaxMembers:1,
      MaxVPN:0,
      Pricing:{"1":0,"12":0,"24":0},
      Amount:0
    }
  ]);

  React.useEffect(() => {
    //Grabing the packages and store them inside the state if the call returns data
   getPackages().then( result => {
      result.json().then(res => res.Plans.map(plans => {
        setLoading(false);
        return plans.Type===1 && plans.MaxDomains > 0  && plans.Name !== 'business' &&  setPlans(plan=> plan.concat(plans))
      })
     ).catch(err => setError(`Error Fetching data: ${err}`));
   })
  }, []);

    return (
      plans.map(({ID, Name, MaxMembers, Pricing, MaxAddresses, MaxDomains, MaxSpace, MaxVPN}) => { 
        
        const MaxUsers = Name === 'professional' ? '1 - 5000 user*' : MaxMembers > 1 ? `${MaxMembers} users` : `${MaxMembers} user`;
        const timePeriod = period === "1" ? "Month" : period === "12" ? "Year" : "2 Years" ;

        return ( 
          <React.Fragment key={ID}>
            {error ? error : 
              loading ? <p>Fetching data</p> :

              <div className="pricetable">
                
                {Name === "plus" && <span className="popular"> Most popular</span>}
                <h4 className="pricetable__name">{Name}</h4>
                <p> 
                  {currency} 
                  <span className="pricetable__currency"> {(convertAmount(Pricing[period]) / period).toFixed(2) }</span>/mo
                </p> 
                <p className="small"> {Pricing[12] > 0 && `Billed as ${currency}${convertAmount(Pricing[period])} per ${timePeriod}`}</p>
                <p className="pricetable__description"> {extra[Name].description} </p>

                <ul className="pricetable__list">
                  <li>{MaxUsers}</li>
                  <li>{convertSpace(MaxSpace)} Storage{optional(Name)}</li>
                  <li> {MaxAddresses} addresses{optional(Name)} </li>
                  <li> {MaxDomains > 0 ? `Supports ${MaxDomains} domain` : 'No domain support'}{optional(Name)}</li>
                  {keyExists("features", extra[Name]) ? <li> {keyExists("features", extra[Name])} </li> : ''} 
                  {keyExists("support", extra[Name]) ? <li> {keyExists("support", extra[Name])} </li> : ''} 
                  <li> {MaxVPN > 0 ? "Includes ProtonVPN" : "ProtonVPN (optional)*"}</li>
                </ul>

                <button> Select </button>

              </div>
              
          }
          </React.Fragment>
        )
      })
    );
  
}


PriceList.propTypes = {
  currency: PropTypes.string.isRequired,
  period: PropTypes.string.isRequired,
  extra: PropTypes.object.isRequired
}