import React from 'react';

export default function Dropdown({list, onChange}) {   
    return (
        <select className="dropdown" onChange={(e) => onChange(e.currentTarget.value)}>
            {list.map(item => <option key={item.value} value={item.value}>{item.name}</option>)}
        </select>
    )
}