import React from "react"
import PriceList from '../components/PriceList'
import Dropdown from '../components/Dropdown'
import { graphql } from 'gatsby'

import '../assets/scss/_main.scss';

export default function Home({data}) {

  const [currency, setCurrency] = React.useState("€");
  const [period, setPeriod] = React.useState("1");

  return (
    <React.Fragment>
      <div className="header">
          <h2>Plans & Prices</h2>
          <div className="header__filter">
            <Dropdown onChange={(period) => setPeriod(period)} list={data.site.siteMetadata.timePeriod}/>
            <Dropdown onChange={(currency) => setCurrency(currency)} list={data.site.siteMetadata.currencies}/>
          </div>
        </div>
        <div className="pricelist">
          <PriceList currency={currency} period={period} extra={data.site.siteMetadata.packages} />
        </div>
    </React.Fragment>
  )
}


export const query = graphql`
  query HomePageQuery {
    site {
      siteMetadata {
        timePeriod {
          value
          name
        }
        currencies {
          name
          value
        }
        packages {
          free {
            description
          }
          plus {
            features
            description
          }
          professional {
            features
            description
          }
          visionary {
            description
            features
            support
          }
        }
      }
    }
  }
`