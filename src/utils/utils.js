/**
 * Adds a dot "." after the last 2 digits of a number 
 *
 * @param {number} amount The number that should be converted.
 * @return {number} the number converted to 2 decimal places.
 */
export function convertAmount(amount) {
    return amount = (amount-(amount%100))/100;
}

/**
 * Converts bytes to Mb or Gb
 *
 * @param {number} bytes - The amount of bytes that should be converted.
 * @return {string}  -  a string with the converted value followed by the name of the memory size.
 */
export function convertSpace(bytes) {
    return bytes / Math.pow(1024,3) < 1 ? `${bytes / Math.pow(1024,2)} Mb` : `${bytes / Math.pow(1024,3)} Gb`;
}

/**
 * Checks if a specific option is part of the 'professional' or 'plus' packages.
 *
 * @param {string} option - The package option that we should check for.
 * @return {string}  -  returns an asterisk "*" to the selected option
 */
export function optional(option) {
    return (option === 'professional' || option === 'plus') ? "*" : null;
}

/**
 * Checks if a specific key exists in the object and it returns it
 *
 * @param {string} key - The key that we have to look for.
 * @param {object} object - The object where the function is looking for the key.
 * @return {string}  -  returns the value of the key which was found in the object
 */
export function keyExists(key, object) { 
    return key in object ? object[key] : null;
}